# [Culpeo](http://byzan.iubang.com:666)
> a kind of fox 🦊  

Byzan's GitLab extensions to achieve more using [GitLab API](http://git.iubang.com/help/api/README.md)
> created with command `ng new culpeo --skip-tests --skip-install --style scss --minimal true --prefix clp --routing true`

## Abilities
* [Query Issue](http://byzan.iubang.com:666/query)  
search issues with a keyword by filtering title and description of an issue after getting all issues

* [Transfer Issue](http://byzan.iubang.com:666/transfer)  
transfer issues from a milestone to another in clicks

* [More](http://byzan.iubang.com:666/more)  
coming soon...

## Append to GitLab Navbar

`/opt/gitlab/embedded/service/gitlab-rails/app/views/layouts/nav/_dashboard.html.haml#L33`

```haml
  - if dashboard_nav_link?(:culpeo)
    = nav_link(html_options: { class: ["d-none d-xl-block", ("d-lg-block" unless has_extra_nav_icons?)] }) do
      = link_to 'https://service.byzan.iubang.com', target: '_blank', class: 'dashboard-shortcuts-culpeo', title: _('A Culpeo says you can achieve more ~') do
        = _('Culpeo')
```

`/opt/gitlab/embedded/service/gitlab-rails/app/helpers/dashboard_helper.rb#L27`

```ruby
  def get_dashboard_nav_links
    links = [:projects, :groups, :snippets, :culpeo]

    if can?(current_user, :read_cross_project)
      links += [:activity, :milestones]
    end

    links
  end
```