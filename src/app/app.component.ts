import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { ConfigService, Configuration } from './@core/services';

@Component({
    selector: 'clp-app',
    templateUrl: './app.component.html',
    styles: [],
})
export class AppComponent {

    public modalVisibility: boolean = false;

    public currentConfig: Configuration;

    public constructor(
        public router: Router,
        private nzMessage: NzMessageService,
        private config: ConfigService,
    ) {
        this.currentConfig = this.config.getConfig();
        if (!this.currentConfig || !this.currentConfig.url || !this.currentConfig.token) {
            this.modalVisibility = true;
        }
        this.config.onConfig().subscribe(conf => {
            if (conf) {
                this.currentConfig = conf;
            }
        });
    }

    public saveConfig(): void {
        let confRes: boolean | string = this.config.setConfig(this.currentConfig);

        if (confRes == true) {
            this.modalVisibility = false;
        } else {
            if (confRes == false) {
                this.nzMessage.error('Save FAILED!');
            } else {
                this.nzMessage.error(`Save FAILED: ${confRes}`);
            }
        }
    }

    public hideModal(): void {
        if (!this.currentConfig || !this.currentConfig.url || !this.currentConfig.token) {
            this.nzMessage.error('Please full fill all the blanks!');
            return;
        } else {
            this.modalVisibility = false;
        }

    }
}
