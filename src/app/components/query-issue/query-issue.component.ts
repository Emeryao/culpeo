import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IssueService, LabelService, UserService } from 'app/@core/services';
import { Group, Issue, IssueQueryScope, IssueQueryState, Label, Project, User } from 'app/models/issue.model';
import { NzMessageService, NzModalService, NzTreeNode } from 'ng-zorro-antd';
import { forkJoin } from 'rxjs';

@Component({
    selector: 'clp-query-issue',
    templateUrl: './query-issue.component.html',
    styleUrls: ['./query-issue.component.scss'],
})
export class QueryIssueComponent implements OnInit {
    public issueList: Array<Issue> = new Array<Issue>();

    public selectedIssueList: Array<Issue> = new Array<Issue>();

    public labelList: Array<Label> = new Array<Label>();

    public queryForm: FormGroup;

    public isLoading: boolean = false;

    /**groups and projects */
    public gpList: Array<NzTreeNode> = new Array<NzTreeNode>();

    public labelNodes: Array<NzTreeNode> = new Array<NzTreeNode>();

    public antiLabelNodes: Array<NzTreeNode> = new Array<NzTreeNode>();

    public orLabelNodes: Array<NzTreeNode> = new Array<NzTreeNode>();

    public users: Array<User> = new Array<User>();

    private systemUserNames: Array<string> = ['support-bot', 'ghost', 'root', 'guest', 'master'];

    private defaultUsers: Array<User> = new Array<User>();

    public constructor(
        private http: HttpClient,
        private formBuilder: FormBuilder,
        private issueService: IssueService,
        private labelService: LabelService,
        private userService: UserService,
        private nzModal: NzModalService,
        private nzMessage: NzMessageService,
    ) {
        this.queryForm = this.formBuilder.group({
            state: ['opened', [Validators.required]],
            projects: [[]],
            author: [''],
            keyword: [''],
            scope: ['all', [Validators.required]],
            withLabels: [[]],
            withoutLabels: [[]],
            orLabels: [[]],
        });
    }

    public async ngOnInit(): Promise<void> {
        let allGroups: Array<Group> = await this.http.get<Array<Group>>(`api/v4/groups?order_by=id&sort=asc`).toPromise();

        for (const group of allGroups) {
            let node: NzTreeNode = new NzTreeNode({ title: `🎁 ${group.full_name}`, key: `group:${group.id}` });
            node.origin = group;
            this.gpList.push(node);
            let projects: Array<Project> = await this.http.get<Array<Project>>(`api/v4/groups/${group.id}/projects?order_by=id&sort=asc`).toPromise();
            if (projects && projects.length > 0) {
                for (const proj of projects) {
                    let pNode: NzTreeNode = new NzTreeNode({ title: `🎀 ${proj.name}`, key: `project:${proj.id}`, isLeaf: true }, node);
                    pNode.origin = proj;
                    node.children.push(pNode);
                    if (!this.labelList || this.labelList.length < 1) {
                        this.labelList = await this.labelService.getByProject(proj.id);
                    }
                }
            }
        }

        if (this.labelList && this.labelList.length > 0) {
            this.labelList.sort((x, y) => {
                let xArr: Array<string> = x.name.split(' ');
                let yArr: Array<string> = y.name.split(' ');
                if (xArr[1] && yArr[1]) {
                    return xArr[1].localeCompare(yArr[1]);
                } else {
                    return x.id - y.id;
                }
            });

            for (const label of this.labelList) {
                let node: NzTreeNode = new NzTreeNode({ title: label.name, key: label.name, isLeaf: true });
                node.origin = label;
                this.labelNodes.push(node);

                let antiNode: NzTreeNode = new NzTreeNode({ title: label.name, key: label.name, isLeaf: true });
                antiNode.origin = label;
                this.antiLabelNodes.push(antiNode);

                let orNode: NzTreeNode = new NzTreeNode({ title: label.name, key: label.name, isLeaf: true });
                orNode.origin = label;
                this.orLabelNodes.push(antiNode);
            }
        }

        let users: Array<User> = await this.userService.getUsers();

        if (users && users.length > 0) {
            this.defaultUsers = users.filter(u => this.systemUserNames.indexOf(u.username) < 0).sort((x, y) => x.username.localeCompare(y.username));
            if (this.defaultUsers && this.defaultUsers.length > 0) {
                this.users = this.defaultUsers;
            }
        }

    }

    public async onProjectChange(e: Array<string>): Promise<void> {
        if (e && e.length > 0) {
            let projectNodes: Array<string> = e.filter(n => n.indexOf('project:') >= 0);
            let projectUsers: Array<User> = new Array<User>();

            for (const proj of projectNodes) {
                let projUsers: Array<User> = await this.userService.getProjectUsers(parseInt(proj.split(':')[1], 10));
                for (const pUser of projUsers) {
                    if (projectUsers.findIndex(u => u.id == pUser.id) < 0) {
                        projectUsers.push(pUser);
                    }
                }
            }

            projectUsers = projectUsers.filter(u => this.systemUserNames.indexOf(u.username) < 0).sort((x, y) => x.username.localeCompare(y.username));
            if (projectUsers && projectUsers.length > 0) {
                this.users = projectUsers;
            } else {
                this.users = this.defaultUsers;
            }
        } else {
            this.users = this.defaultUsers;
        }
    }

    public async submitForm(): Promise<void> {
        for (const i in this.queryForm.controls) {
            if (this.queryForm.controls.hasOwnProperty(i)) {
                this.queryForm.controls[i].markAsDirty();
                this.queryForm.controls[i].updateValueAndValidity();
            }
        }
        if (!this.queryForm.valid) { return; }

        await this.searchIssue(this.queryForm.value);
    }

    public clearKeyword(): void {
        this.queryForm.controls['keyword'].setValue('');
    }

    public onIssueSelect(e: boolean, iss: Issue): void {
        if (e) {
            this.selectedIssueList.push(iss);
        } else {
            this.selectedIssueList.splice(this.selectedIssueList.findIndex(i => i.id == iss.id), 1);
        }
    }

    public onPageIndexChange(): void {
        this.selectedIssueList = new Array<Issue>();
    }

    public updateIssueList(action: 'close' | 'reopen'): void {
        if (this.selectedIssueList.length > 0) {
            this.nzModal.confirm({
                nzTitle: 'ARE YOU SURE?',
                nzCancelText: 'OK',
                nzOkText: 'Cancel',
                nzOnCancel: async () => {
                    let updateReqs: Array<Promise<boolean>> = new Array<Promise<boolean>>();
                    for (const issue of this.selectedIssueList) {
                        if (action == 'close' && issue.state == 'opened') {
                            updateReqs.push(this.issueService.updateIssueState(issue, action));
                        } else if (action == 'reopen' && issue.state == 'closed') {
                            updateReqs.push(this.issueService.updateIssueState(issue, action));
                        }
                    }
                    await forkJoin(updateReqs).toPromise();

                    await this.searchIssue(this.queryForm.value);

                    this.selectedIssueList = new Array<Issue>();
                },
            });
        } else {
            this.nzMessage.info('Please select issues!');
        }
    }

    private async searchIssue({ keyword, state, projects, scope, withLabels, withoutLabels, orLabels, author }: { keyword: string; state: IssueQueryState; scope: IssueQueryScope; projects: Array<string>; withLabels: Array<string>; withoutLabels: Array<string>; orLabels: Array<string>; author: number }): Promise<void> {
        this.isLoading = true;

        let queriedList: Array<Issue> = new Array<Issue>();

        if (!projects || projects.length < 1) {
            queriedList = await this.issueService.getAll(state, scope, withLabels, author);
        } else {
            for (const proj of projects) {
                let projArr: Array<string> = proj.split(':');
                let type: string = projArr[0];
                let id: number = parseInt(projArr[1], 10);
                if (type == 'group') {
                    let resList: Array<Issue> = await this.issueService.getByGroup(id, state, scope, withLabels, author);
                    queriedList.push(...resList);
                } else if (type == 'project') {
                    let resList: Array<Issue> = await this.issueService.getByProject(id, state, scope, withLabels, author);
                    queriedList.push(...resList);
                }
            }
        }

        let filteredList: Array<Issue> = queriedList;

        if (keyword) {
            filteredList = filteredList.filter(i => (i.title && i.title.indexOf(keyword) >= 0) || (i.description && i.description.indexOf(keyword) >= 0));
        }

        if (withoutLabels && withoutLabels.length > 0) {
            if (withLabels && withLabels.length > 0) {
                let withLabelSet: Set<string> = new Set(withLabels);
                withoutLabels = withoutLabels.filter(l => !withLabelSet.has(l));
            }

            filteredList = filteredList.filter(i => {
                let withoutLabelSet: Set<string> = new Set(withoutLabels);
                return i.labels.findIndex(il => withoutLabelSet.has(il)) < 0;
            });
        }

        if (orLabels && orLabels.length > 0) {
            filteredList = filteredList.filter(i => {
                return orLabels.filter(l => i.labels.indexOf(l) >= 0).length > 0;
            });
        }

        filteredList.forEach(i => {
            let url: URL = new URL(i.web_url);
            i.reference = url.pathname.replace('/issues/', '#').substr(1);
        });

        this.issueList = filteredList;

        this.isLoading = false;
    }
}
