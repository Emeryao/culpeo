import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ConfigService, Configuration } from 'app/@core/services';
import { Issue, Milestone } from 'app/models/issue.model';
import { NzMessageService, TransferChange, TransferItem } from 'ng-zorro-antd';

@Component({
    selector: 'clp-transfer-issue',
    templateUrl: './transfer-issue.component.html',
    styleUrls: ['./transfer-issue.component.scss'],
})
export class TransferIssueComponent implements OnInit {
    public state: string = 'opened';

    public activeMilestones: Array<Milestone> = new Array<Milestone>();

    public closedMilestones: Array<Milestone> = new Array<Milestone>();

    public currentConfig: Configuration;

    public transList: Array<TransferItem> = new Array<TransferItem>();

    public sourceMilestone: Milestone;

    public targetMilestone: Milestone;

    public constructor(
        private http: HttpClient,
        private nzMessage: NzMessageService,
        private config: ConfigService,
    ) {
        this.currentConfig = this.config.getConfig();
    }

    public async ngOnInit(): Promise<void> {
        let resList: Array<Milestone> = new Array<Milestone>();

        let page: number = 1;
        while (page >= 0) {
            try {
                let res: HttpResponse<Array<Milestone>> = await this.http.get<Array<Milestone>>(encodeURI(`${this.currentConfig.url}/api/v4/groups/Byzan/milestones?per_page=100`), {
                    observe: 'response',
                }).toPromise();

                if (!res.ok) {
                    return;
                }
                page = parseInt(res.headers.get('x-next-page'), 10);
                resList.push(...res.body);
            } catch (error) {
                this.nzMessage.error(`${(error as HttpErrorResponse).message}`);
                return;
            }
        }

        this.activeMilestones = resList.filter(m => m.state == 'active');
        this.closedMilestones = resList.filter(m => m.state == 'closed');
    }

    public async getIssues(): Promise<void> {
        if (!this.sourceMilestone || !this.targetMilestone) {
            this.nzMessage.info('Please select Milestones');
            return;
        } else if (this.sourceMilestone == this.targetMilestone) {
            this.nzMessage.info('Please select different Milestones');
            return;
        }

        this.transList = new Array<TransferItem>();

        let resList: Array<Issue> = new Array<Issue>();
        let page: number = 1;
        while (page >= 0) {
            try {
                let res: HttpResponse<Array<Issue>> = await this.http.get<Array<Issue>>(encodeURI(`${this.currentConfig.url}/api/v4/groups/Byzan/issues?scope=all&state=${this.state}&milestone=${this.sourceMilestone.title}&per_page=100&page=${page}`), {
                    observe: 'response',
                }).toPromise();
                if (!res.ok) {
                    return;
                }
                page = parseInt(res.headers.get('x-next-page'), 10);
                resList.push(...res.body);
            } catch (error) {
                this.nzMessage.error(`${(error as HttpErrorResponse).message}`);
                return;
            }
        }
        let sourceTransItems: Array<TransferItem> = new Array<TransferItem>();
        resList.forEach(i => {
            sourceTransItems.push({
                title: i.title,
                direction: 'left',
                issue: i,
            });
        });

        resList = new Array<Issue>();
        page = 1;
        while (page >= 0) {
            try {
                let res: HttpResponse<Array<Issue>> = await this.http.get<Array<Issue>>(encodeURI(`${this.currentConfig.url}/api/v4/groups/Byzan/issues?scope=all&state=${this.state}&milestone=${this.targetMilestone.title}&per_page=100&page=${page}`), {
                    observe: 'response',
                }).toPromise();
                if (!res.ok) {
                    return;
                }
                page = parseInt(res.headers.get('x-next-page'), 10);
                resList.push(...res.body);
            } catch (error) {
                this.nzMessage.error(`${(error as HttpErrorResponse).message}`);
                return;
            }
        }
        let targetTransItems: Array<TransferItem> = new Array<TransferItem>();
        resList.forEach(i => {
            targetTransItems.push({
                title: i.title,
                direction: 'right',
                issue: i,
            });
        });

        this.transList = [...sourceTransItems, ...targetTransItems];

    }

    public onTransfer(e: TransferChange): void {
        if (e.from == 'left' && e.to == 'right') {
            e.list.forEach(item => {
                this.http.put(encodeURI(`${this.currentConfig.url}/api/v4/projects/${item.issue.project_id}/issues/${item.issue.iid}?milestone_id=${this.targetMilestone.id}`), undefined).toPromise();
            });
        }
        if (e.from == 'right' && e.to == 'left') {
            e.list.forEach(item => {
                this.http.put(encodeURI(`${this.currentConfig.url}/api/v4/projects/${item.issue.project_id}/issues/${item.issue.iid}?milestone_id=${this.sourceMilestone.id}`), undefined).toPromise();
            });
        }
    }

}
