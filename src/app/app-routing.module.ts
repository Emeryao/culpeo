import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent, NotFoundComponent, QueryIssueComponent, TransferIssueComponent } from './components';

const routes: Routes = [{
    path: '',
    pathMatch: 'full',
    redirectTo: 'home',
}, {
    path: 'home',
    component: HomeComponent,
}, {
    path: 'query',
    component: QueryIssueComponent,
}, {
    path: 'transfer',
    component: TransferIssueComponent,
}, {
    path: '404',
    component: NotFoundComponent,
}, {
    path: '**',
    redirectTo: '404',
}];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule { }
