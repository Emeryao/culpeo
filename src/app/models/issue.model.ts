export type IssueQueryState = 'opened' | 'closed' | 'all';

export type IssueQueryScope = 'created_by_me' | 'assigned_to_me' | 'all';

export interface Issue {
    assignee?: User;
    assignees?: Array<User>;
    author?: User;
    closed_at?: string;
    closed_by?: User;
    confidential?: boolean;
    created_at?: string;
    description?: string;
    discussion_locked?: boolean;
    downvotes?: number;
    due_date?: string;
    id?: number;
    iid?: number;
    labels?: Array<string>;
    milestone?: Milestone;
    project_id?: number;
    reference?: string;
    state?: 'opened' | 'closed';
    time_stats?: {
        human_time_estimate?: any;
        human_total_time_spent?: any;
        time_estimate?: number;
        total_time_spent?: number;
    };
    title?: string;
    updated_at?: string;
    upvotes?: number;
    user_notes_count?: number;
    web_url?: string;
    weight?: number;
}

export interface User {
    avatar_url?: string;
    id?: number;
    name?: string;
    state?: string;
    username?: string;
    web_url?: string;
}

export interface Milestone {
    created_at?: string;
    description?: string;
    due_date?: string;
    group_id?: number;
    id?: number;
    iid?: number;
    start_date?: string;
    state?: string;
    title?: string;
    updated_at?: string;
    web_url?: string;
}

export interface Group {
    avatar_url?: string;
    description?: string;
    file_template_project_id?: number;
    full_name?: string;
    full_path?: string;
    id: number;
    ldap_access?: any;
    ldap_cn?: any;
    lfs_enabled?: boolean;
    name?: string;
    parent_id: number | null;
    path?: string;
    request_access_enabled?: boolean;
    visibility?: string;
    web_url?: string;
}

export interface Project {
    _links: Links;
    approvals_before_merge: number;
    archived: boolean;
    avatar_url: string;
    ci_config_path?: any;
    container_registry_enabled: boolean;
    created_at: string;
    creator_id: number;
    default_branch: string;
    description: string;
    external_authorization_classification_label?: any;
    forks_count: number;
    http_url_to_repo: string;
    id: number;
    import_status: string;
    issues_enabled: boolean;
    jobs_enabled: boolean;
    last_activity_at: string;
    lfs_enabled: boolean;
    merge_method: string;
    merge_requests_enabled: boolean;
    mirror: boolean;
    name: string;
    name_with_namespace: string;
    namespace: Namespace;
    only_allow_merge_if_all_discussions_are_resolved: boolean;
    only_allow_merge_if_pipeline_succeeds: boolean;
    open_issues_count: number;
    packages_enabled: boolean;
    path: string;
    path_with_namespace: string;
    printing_merge_request_link_enabled: boolean;
    public_jobs: boolean;
    readme_url: string;
    request_access_enabled: boolean;
    resolve_outdated_diff_discussions: boolean;
    shared_runners_enabled: boolean;
    shared_with_groups: Array<any>;
    snippets_enabled: boolean;
    ssh_url_to_repo: string;
    star_count: number;
    tag_list: Array<any>;
    visibility: string;
    web_url: string;
    wiki_enabled: boolean;
}

export interface Links {
    events: string;
    issues: string;
    labels: string;
    members: string;
    merge_requests: string;
    repo_branches: string;
    self: string;
}

export interface Namespace {
    full_path: string;
    id: number;
    kind: string;
    name: string;
    parent_id?: any;
    path: string;
}

export interface Label {
    id: number;
    name: string;
    color: string;
    description: string;
    open_issues_count: number;
    closed_issues_count: number;
    open_merge_requests_count: number;
    priority?: any;
    subscribed: boolean;
}
