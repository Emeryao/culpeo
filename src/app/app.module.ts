import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { default as en } from '@angular/common/locales/en';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N, NgZorroAntdModule, en_US } from 'ng-zorro-antd';
import { CoreModule } from './@core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotFoundComponent, QueryIssueComponent, TransferIssueComponent } from './components';
import { HomeComponent } from './components/@home/home.component';

registerLocaleData(en);

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        NotFoundComponent,
        QueryIssueComponent,
        TransferIssueComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgZorroAntdModule,
        CoreModule,
    ],
    providers: [{ provide: NZ_I18N, useValue: en_US }],
    bootstrap: [AppComponent],
})
export class AppModule { }
