import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Issue, IssueQueryScope, IssueQueryState } from 'app/models/issue.model';
import { NzMessageService } from 'ng-zorro-antd';

@Injectable({ providedIn: 'root' })
export class IssueService {
    public constructor(
        private http: HttpClient,
        private nzMessage: NzMessageService,
    ) { }

    public async getAll(state: IssueQueryState, scope: IssueQueryScope, labels: Array<string> = [], authorId: number = 0): Promise<Array<Issue>> {

        let retList: Array<Issue> = new Array<Issue>();

        let page: number = 1;
        while (page >= 0) {
            try {
                let res: HttpResponse<Array<Issue>> = await this.http.get<Array<Issue>>(encodeURI(`api/v4/issues?scope=${scope}&state=${state}&labels=${labels.join(',')}&author_id=${authorId ? authorId : ''}&per_page=100&page=${page}`), {
                    observe: 'response',
                }).toPromise();
                if (!res.ok) {
                    return retList;
                }
                page = parseInt(res.headers.get('x-next-page'), 10);
                let resList: Array<Issue> = res.body;
                retList.push(...resList);
            } catch (error) {
                this.nzMessage.error(`${(error as HttpErrorResponse).message}`);
                return retList;
            }
        }

        return retList;
    }

    public async getByGroup(groupId: number, state: IssueQueryState, scope: IssueQueryScope, labels: Array<string> = [], authorId: number = 0): Promise<Array<Issue>> {

        let retList: Array<Issue> = new Array<Issue>();

        let page: number = 1;
        while (page >= 0) {
            try {
                let res: HttpResponse<Array<Issue>> = await this.http.get<Array<Issue>>(encodeURI(`api/v4/groups/${groupId}/issues?scope=${scope}&state=${state}&labels=${labels.join(',')}&author_id=${authorId ? authorId : ''}&per_page=100&page=${page}`), {
                    observe: 'response',
                }).toPromise();
                if (!res.ok) {
                    return retList;
                }
                page = parseInt(res.headers.get('x-next-page'), 10);
                let resList: Array<Issue> = res.body;
                retList.push(...resList);
            } catch (error) {
                this.nzMessage.error(`${(error as HttpErrorResponse).message}`);
                return retList;
            }
        }

        return retList;
    }

    public async getByProject(projectId: number, state: IssueQueryState, scope: IssueQueryScope, labels: Array<string> = [], authorId: number = 0): Promise<Array<Issue>> {
        let retList: Array<Issue> = new Array<Issue>();

        let page: number = 1;
        while (page >= 0) {
            try {
                let res: HttpResponse<Array<Issue>> = await this.http.get<Array<Issue>>(encodeURI(`api/v4/projects/${projectId}/issues?scope=${scope}&state=${state}&labels=${labels.join(',')}&author_id=${authorId ? authorId : ''}&per_page=100&page=${page}`), {
                    observe: 'response',
                }).toPromise();
                if (!res.ok) {
                    return retList;
                }
                page = parseInt(res.headers.get('x-next-page'), 10);
                let resList: Array<Issue> = res.body;
                retList.push(...resList);
            } catch (error) {
                this.nzMessage.error(`${(error as HttpErrorResponse).message}`);
                return retList;
            }
        }

        return retList;
    }

    public async updateIssueState(issue: Issue, action: 'close' | 'reopen'): Promise<boolean> {
        try {
            await this.http.put(encodeURI(`api/v4/projects/${issue.project_id}/issues/${issue.iid}?state_event=${action}`), null).toPromise();
            return true;
        } catch (error) {
            this.nzMessage.error(`${(error as HttpErrorResponse).message}`);
            return false;
        }

    }

}
