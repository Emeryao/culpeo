import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export interface Configuration {
    url: string;
    token: string;
}

@Injectable({ providedIn: 'root' })
export class ConfigService {

    private storageKey: string = 'clp-config';

    private config$: BehaviorSubject<Configuration> = new BehaviorSubject<Configuration>(null);

    public constructor() {
        let storedConfig: Configuration = { url: 'http://git.iubang.com', token: '' };
        let storedConfigString: string = localStorage.getItem(this.storageKey);
        if (storedConfigString) {
            storedConfig = JSON.parse(storedConfigString);
        } else {
            localStorage.setItem(this.storageKey, JSON.stringify(storedConfig));
        }
        this.config$.next(storedConfig);
    }

    public onConfig(): Observable<Configuration> {
        return this.config$.asObservable();
    }

    public getConfig(): Configuration {
        return this.config$.getValue();
    }

    public setConfig(config: Configuration): boolean | string {
        if (!config || !config.url || !config.token) {
            return 'Please full fill all the blanks';
        }
        let currentConfig: Configuration = this.config$.getValue();
        try {
            let url: URL = new URL(config.url);
            if (url.protocol != 'https:' && url.protocol != 'http:') {
                return 'URL protocol not supported';
            }
            currentConfig.url = url.origin;
        } catch (error) {
            if (error instanceof TypeError) {
                return (error as TypeError).message;
            } else {
                return false;
            }
        }

        currentConfig.token = config.token;
        localStorage.setItem(this.storageKey, JSON.stringify(config));
        this.config$.next(currentConfig);
        return true;
    }
}
