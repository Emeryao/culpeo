import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Label } from 'app/models/issue.model';
import { NzMessageService } from 'ng-zorro-antd';

@Injectable({ providedIn: 'root' })
export class LabelService {
    public constructor(
        private http: HttpClient,
        private nzMessage: NzMessageService,
    ) { }

    public async getByProject(projectId: number): Promise<Array<Label>> {
        let retList: Array<Label> = new Array<Label>();

        let page: number = 1;
        while (page >= 0) {
            try {
                let res: HttpResponse<Array<Label>> = await this.http.get<Array<Label>>(encodeURI(`api/v4/projects/${projectId}/labels?per_page=100&page=${page}`), {
                    observe: 'response',
                }).toPromise();
                if (!res.ok) {
                    return retList;
                }
                page = parseInt(res.headers.get('x-next-page'), 10);
                let resList: Array<Label> = res.body;
                retList.push(...resList);
            } catch (error) {
                this.nzMessage.error(`${(error as HttpErrorResponse).message}`);
                return retList;
            }
        }

        return retList;
    }

}
