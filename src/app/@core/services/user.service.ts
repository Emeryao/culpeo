import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'app/models/issue.model';
import { NzMessageService } from 'ng-zorro-antd';

@Injectable({ providedIn: 'root' })
export class UserService {
    public constructor(
        private http: HttpClient,
        private nzMessage: NzMessageService,
    ) { }

    public async getUsers(): Promise<Array<User>> {
        let retList: Array<User> = new Array<User>();

        let page: number = 1;
        while (page >= 0) {
            try {
                let res: HttpResponse<Array<User>> = await this.http.get<Array<User>>(encodeURI(`api/v4/users?per_page=100&page=${page}`), {
                    observe: 'response',
                }).toPromise();

                if (!res.ok) {
                    return retList;
                }
                page = parseInt(res.headers.get('x-next-page'), 10);
                let resList: Array<User> = res.body;
                retList.push(...resList);
            } catch (error) {
                this.nzMessage.error(`${(error as HttpErrorResponse).message}`);
                return retList;
            }
        }

        return retList;
    }

    public async getProjectUsers(projectId: number): Promise<Array<User>> {
        let retList: Array<User> = new Array<User>();

        let page: number = 1;
        while (page >= 0) {
            try {
                let res: HttpResponse<Array<User>> = await this.http.get<Array<User>>(encodeURI(`api/v4/projects/${projectId}/users?per_page=100&page=${page}`), {
                    observe: 'response',
                }).toPromise();

                if (!res.ok) {
                    return retList;
                }
                page = parseInt(res.headers.get('x-next-page'), 10);
                let resList: Array<User> = res.body;
                retList.push(...resList);
            } catch (error) {
                this.nzMessage.error(`${(error as HttpErrorResponse).message}`);
                return retList;
            }
        }

        return retList;
    }

}
