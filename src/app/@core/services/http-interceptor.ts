import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService, Configuration } from './config.service';

@Injectable()
export class GitLabApiInterceptor implements HttpInterceptor {

    private config: ConfigService;

    public constructor(
        private injector: Injector,
    ) { }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (!this.config) {
            this.config = this.injector.get<ConfigService>(ConfigService);
        }

        let headers: { [name: string]: string | Array<string> } = {};

        let url: string = req.url;

        let config: Configuration = this.config.getConfig();

        if (config) {
            if (config.token) {
                headers['PRIVATE-TOKEN'] = config.token;
            }
            if (config.url) {
                if (url.indexOf('http') < 0 && url.indexOf(config.url) < 0) {
                    url = `${config.url}/${url}`;
                }
            }
        }

        let authReq: HttpRequest<any> = req.clone({ setHeaders: headers, url });
        return next.handle(authReq);
    }
}
