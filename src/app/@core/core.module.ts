import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { GitLabApiInterceptor } from './services/http-interceptor';

@NgModule({
    providers: [{
        provide: HTTP_INTERCEPTORS,
        useClass: GitLabApiInterceptor,
        multi: true,
    }],
})
export class CoreModule { }
