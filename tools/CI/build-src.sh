#!/bin/bash

SHOULD_BUILD=false

if [ -z $DEPLOY_CARGO_NAME ]
then
    DEPLOY_CARGO_NAME=byz-culpeo-dist
fi

if [ $CI_PIPELINE_SOURCE == web ]
then
    echo "BUILD WILL BE EXECUTED since \$CI_PIPELINE_SOURCE equals \"web\" ie. this pineline($CI_PIPELINE_ID)/job($CI_JOB_ID) was initiated from GitLab web portal manually"
    SHOULD_BUILD=true
fi

if [ $SHOULD_BUILD == false ]
then
    GUEST_API_PRIVATE_TOKEN=vGJtWyW3yUamyv1xqr1K
    GITLAB_API_URL=${CI_PROJECT_URL//$CI_PROJECT_PATH/"api/v4/projects"}

    echo -e "\nRUNNING: \"$ LAST_SUCCESS_PIPELINE=\$(curl -s --header \"PRIVATE-TOKEN: \$GUEST_API_PRIVATE_TOKEN\" \"$GITLAB_API_URL/$CI_PROJECT_ID/pipelines?status=success&ref=$CI_COMMIT_REF_NAME\" | jq \".[0]\")\""
    LAST_SUCCESS_PIPELINE=$(curl -s --header "PRIVATE-TOKEN: $GUEST_API_PRIVATE_TOKEN" "$GITLAB_API_URL/$CI_PROJECT_ID/pipelines?status=success&ref=$CI_COMMIT_REF_NAME" | jq ".[0]")
    if [ $? -eq 0 ] && [ "$LAST_SUCCESS_PIPELINE" != "null" ]
    then
        echo -e "last success pipeline:\n$LAST_SUCCESS_PIPELINE"
        echo -e "\nRUNNING: \"$ LAST_COMMIT_ID=\$(echo \$LAST_SUCCESS_PIPELINE | jq -r \".sha\")\""
        LAST_COMMIT_ID=$(echo $LAST_SUCCESS_PIPELINE | jq -r ".sha")
        echo -e "\nRUNNING: \"$ declare -a MODIFIED_FILES=(\$(git diff --name-only $LAST_COMMIT_ID))\""
        declare -a MODIFIED_FILES=($(git diff --name-only $LAST_COMMIT_ID))
        if [ $? -ne 0 ]
        then
            echo -e "\nERROR RUNNING: \"$ declare -a MODIFIED_FILES=(\$(git diff --name-only $LAST_COMMIT_ID))\""
            exit 1
        fi
    else
        echo -e "\nERROR: runnnig command \"$ LAST_SUCCESS_PIPELINE=\$(curl -s --header \"PRIVATE-TOKEN: \$GUEST_API_PRIVATE_TOKEN\" \"$GITLAB_API_URL$PROJECT_PATH_ENCODED/pipelines?status=success&ref=$CI_COMMIT_REF_NAME\" | jq \".[0]\")\""
        echo -e "\nRUNNING: \"$ declare -a MODIFIED_FILES=(\$(git diff-tree --name-only -r HEAD))\""
        declare -a MODIFIED_FILES=($(git diff-tree --name-only -r HEAD))
    fi

    for i in ${MODIFIED_FILES[@]}
    do
        echo "check for modified file: $i"
        if [[ $i == ".gitlab-ci.yml" || $i == "package.json" || $i =~ ^"src/" || $i =~ ^"tools/CI" ]]
        then
            echo "$i : detected modification BUILD WILL BE EXECUTED"
            SHOULD_BUILD=true
            break
        fi
    done

    if [ $SHOULD_BUILD != true ]
    then
        echo "NO MODIFIED FILES should be build"
        echo "CANCEL the running job in 5 seconds"
        MASTER_API_ACCESS_TOKEN=wR_vbAP5sdUwMUYa7B6T
        sleep 5
        echo -e "\nRUNNING: \"$ curl -s --request POST --header \"PRIVATE-TOKEN: \$MASTER_API_ACCESS_TOKEN\" \"$GITLAB_API_URL/$CI_PROJECT_ID/jobs/$CI_JOB_ID/cancel\""
        curl -s --request POST --header "PRIVATE-TOKEN: $MASTER_API_ACCESS_TOKEN" "$GITLAB_API_URL/$CI_PROJECT_ID/jobs/$CI_JOB_ID/cancel"
        if [ $? -ne 0 ]
        then
            echo -e "\nERROR RUNNING: \"$ curl -s --request POST --header \"PRIVATE-TOKEN: \$MASTER_API_ACCESS_TOKEN\" \"$GITLAB_API_URL/$CI_PROJECT_ID/jobs/$CI_JOB_ID/cancel\""
        fi
        exit 0
    fi
fi

echo -e "\nRUNNING: \"$ node --version\""
node --version
if [ $? -ne 0 ]
then
    echo -e "\nERROR RUNNING: \"$ node --version\""
    exit 1
fi

echo -e "\nRUNNING: \"$ npm --version\""
npm --version
if [ $? -ne 0 ]
then
    echo -e "\nERROR RUNNING: \"$ npm --version\""
    exit 1
fi

echo -e "\nRUNNING: \"$ npm install\""
npm install
if [ $? -ne 0 ]
then
    echo -e "\nERROR RUNNING: \"$ npm install\""
    exit 1
fi

echo -e "\nRUNNING: \"$ npm run build\""
npm run build
if [ $? -ne 0 ]
then
    echo -e "\nERROR RUNNING: \"$ npm run build\""
    exit 1
fi

echo -e "\nRUNNING: \"$ mkdir $DEPLOY_CARGO_NAME\""
mkdir $DEPLOY_CARGO_NAME
if [ $? -ne 0 ]
then
    echo -e "\nERROR RUNNING: \"$ mkdir $DEPLOY_CARGO_NAME\""
    exit 1
fi

echo -e "\nRUNNING: \"$ cp -r ./dist $DEPLOY_CARGO_NAME\""
cp -r ./dist $DEPLOY_CARGO_NAME
if [ $? -ne 0 ]
then
    echo -e "\nERROR RUNNING: \"$ cp -r ./dist $DEPLOY_CARGO_NAME\""
    exit 1
fi

echo -e "\nRUNNING: \"$ cp -r ./tools/CI/* ./tools/CI/.dockerignore $DEPLOY_CARGO_NAME\""
cp -r ./tools/CI/* ./tools/CI/.dockerignore $DEPLOY_CARGO_NAME
if [ $? -ne 0 ]
then
    echo -e "\nERROR RUNNING: \"$ cp -r ./tools/CI/* ./tools/CI/.dockerignore $DEPLOY_CARGO_NAME\""
    exit 1
fi

echo -e "\nRUNNING: \"$ cp -r ./tools/nginx $DEPLOY_CARGO_NAME\""
cp -r ./tools/nginx $DEPLOY_CARGO_NAME
if [ $? -ne 0 ]
then
    echo -e "\nERROR RUNNING: \"$ cp -r ./tools/nginx $DEPLOY_CARGO_NAME\""
    exit 1
fi

echo -e "\nRUNNING: \"$ tar -czf $DEPLOY_CARGO_NAME.tar.gz -C $DEPLOY_CARGO_NAME .\""
tar -czf $DEPLOY_CARGO_NAME.tar.gz -C $DEPLOY_CARGO_NAME .
if [ $? -ne 0 ]
then
    echo -e "\nERROR RUNNING: \"$ tar -czf $DEPLOY_CARGO_NAME.tar.gz -C $DEPLOY_CARGO_NAME .\""
    exit 1
fi

DIST_SIZE_K=$(du -d 0 ./dist | cut -f 1)
DIST_SIZE_M=$(du -d 0 -h ./dist | cut -f 1)

echo -e "\nDIST FOLDER SIZE: ${DIST_SIZE_M}B / ${DIST_SIZE_K}KB"

CARGO_SIZE_K=$(du -d 0 ./$DEPLOY_CARGO_NAME.tar.gz | cut -f 1)
CARGO_SIZE_M=$(du -d 0 -h ./$DEPLOY_CARGO_NAME.tar.gz | cut -f 1)

echo -e "\nCARGO SIZE: ${CARGO_SIZE_M}B / ${CARGO_SIZE_K}KB\n"
