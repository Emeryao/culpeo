#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo -e "RUNNING: $ cd $SCRIPT_DIR"
cd $SCRIPT_DIR
if [ $? -ne 0 ]
then
    echo -e "ERROR RUNNING: \"$ cd $SCRIPT_DIR\""
    exit 1
fi

STACK_NAME=dev-core
IMAGE_NAME=byzan/culpeo
IMAGE_TAG=nightly
REGISTRY_HOST=""

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    --registry)
    REGISTRY_HOST="$2"
    shift
    shift
    ;;
    --stack)
    STACK_NAME="$2"
    shift
    shift
    ;;
    --name)
    IMAGE_NAME="$2"
    shift
    shift
    ;;
    --tag)
    IMAGE_TAG="$2"
    shift
    shift
    ;;
    *)
    shift
    ;;
esac
done

if [ -z $STACK_NAME ]
then
    STACK_NAME=dev-core
fi

echo -e "\nRUNNING: \"$ env \
    TAG=$IMAGE_TAG \
    IMAGE_NAME=$IMAGE_NAME \
    REGISTRY_HOST=$REGISTRY_HOST \
    docker stack up -c ./dev-core_culpeo.yml $STACK_NAME\""

env \
    TAG=$IMAGE_TAG \
    IMAGE_NAME=$IMAGE_NAME \
    REGISTRY_HOST=$REGISTRY_HOST \
    docker stack up -c ./dev-core_culpeo.yml $STACK_NAME
if [ $? -ne 0 ]
then
    echo -e "ERROR RUNNING: \"env \
        TAG=$IMAGE_TAG \
        IMAGE_NAME=$IMAGE_NAME \
        REGISTRY_HOST=$REGISTRY_HOST \
        docker stack up -c ./dev-core_culpeo.yml $STACK_NAME\""
    exit 1
fi
