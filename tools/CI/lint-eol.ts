// tslint:disable:no-console

import { execSync } from 'child_process';
import * as fs from 'fs';
import * as path from 'path';

let guardedExts: Array<string> = ['.ts', '.html', '.scss', '.json', '.js', '.sh', '.md', '.yml'];

let invalidFiles: Array<string> = [];

let fileList: Array<string> = [];

let isAll: boolean = process.argv[2] == '-a';

if (isAll) {
    getFiles(process.cwd(), fileList);
} else {
    let chagnedFiles: string = execSync('git diff-tree --name-only -r HEAD', {
        encoding: 'utf8',
    });
    fileList = chagnedFiles.split('\n');
}

for (const file of fileList) {
    if (!file || !fs.existsSync(file)) {
        continue;
    }
    if (guardedExts.indexOf(path.extname(file).toLowerCase()) < 0) {
        continue;
    }
    let content: string = fs.readFileSync(file, {
        encoding: 'utf8',
    });
    if (content.indexOf('\r\n') >= 0) {
        invalidFiles.push(file);
        if (isAll) {
            content = content.replace(/\r\n/g, '\n');
            fs.writeFileSync(file, content, {
                encoding: 'utf8',
            });
        }

        console.log(`EOL of file: ${file} is invalid ❌`);
    } else {
        if (!isAll) {
            console.log(`EOL of file: ${file} is valid ✔`);
        }
    }
}

if (invalidFiles.length > 0) {
    if (isAll) {
        console.log(`Total EOL fail: ${invalidFiles.length}`);
    }
    process.exit(1);
}

function getFiles(dir: string, filelist: Array<string>): Array<string> {
    filelist = filelist || [];
    let files: Array<string> = fs.readdirSync(dir);
    for (let file of files) {
        if (file == '.git' || file == 'node_modules') {
            continue;
        }
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = getFiles(path.join(dir, file), filelist);
        } else {
            filelist.push(path.join(dir, file));
        }
    }
    return filelist;
}
