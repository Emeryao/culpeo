#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo -e "\nRUNNING: \"$ cd $SCRIPT_DIR\""
cd $SCRIPT_DIR
if [ $? -ne 0 ]
then
    echo -e "\nERROR: runnnig command \"$ cd $SCRIPT_DIR\""
    exit 1
fi

TARGET_ENV=nightly
IMAGE_NAME=byzan/culpeo
BASE_IMAGE=nginx:byz-nightly
CI_COMMIT_SHA=""
CI_PIPELINE_ID=""

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    --env)
    TARGET_ENV="$2"
    shift
    shift
    ;;
    --image-name)
    IMAGE_NAME="$2"
    shift
    shift
    ;;
    --base-image)
    BASE_IMAGE="$2"
    shift
    shift
    ;;
    --ci-commit-sha)
    CI_COMMIT_SHA="$2"
    shift
    shift
    ;;
    --ci-pipeline-id)
    CI_PIPELINE_ID="$2"
    shift
    shift
    ;;
    *)
    shift
    ;;
esac
done

CURRENT_DATE=$(date +%Y%m%d-%H_%M_%S)

BACKUP_IMAGE_NAME=$IMAGE_NAME:$TARGET_ENV-backup-$CURRENT_DATE

echo -e "\nRUNNING: \"$ docker image tag $IMAGE_NAME:$TARGET_ENV $BACKUP_IMAGE_NAME\""
docker image tag $IMAGE_NAME:$TARGET_ENV $BACKUP_IMAGE_NAME
echo -e "\nRUNNING: \"$ docker image rm $IMAGE_NAME:$TARGET_ENV\""
docker image rm $IMAGE_NAME:$TARGET_ENV

echo -e "\nRUNNING: \"$ docker image build --build-arg BASE_IMAGE=$BASE_IMAGE --tag $IMAGE_NAME:$TARGET_ENV --label commit_id=$CI_COMMIT_SHA --label pipeline_id=$CI_PIPELINE_ID .\""
docker image build --build-arg BASE_IMAGE=$BASE_IMAGE --tag $IMAGE_NAME:$TARGET_ENV --label commit_id=$CI_COMMIT_SHA --label pipeline_id=$CI_PIPELINE_ID .
if [ $? -ne 0 ]
then
    echo -e "\nERROR RUNNING: \"$ docker image build --build-arg BASE_IMAGE=$BASE_IMAGE --tag $IMAGE_NAME:$TARGET_ENV --label commit_id=$CI_COMMIT_SHA --label pipeline_id=$CI_PIPELINE_ID .\""

    echo -e "\nRUNNING: \"$ docker image tag $IMAGE_NAME:$TARGET_ENV $BACKUP_IMAGE_NAME\""
    docker image tag $BACKUP_IMAGE_NAME $IMAGE_NAME:$TARGET_ENV
    echo -e "\nRUNNING: \"$ docker image rm $IMAGE_NAME:$TARGET_ENV\""
    docker image rm $BACKUP_IMAGE_NAME

    exit 1
fi

echo -e "\nRUNNING: \"$ docker image rm $BACKUP_IMAGE_NAME -f\""
docker image rm $BACKUP_IMAGE_NAME -f
