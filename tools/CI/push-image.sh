#!/bin/bash

IMAGE_NAME=""
PUSH_IMAGE_VERSION=""
PUSH_LATEST="false"
REGISTRY_HOST=""
IMAGE_TAG=""

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    --name)
    IMAGE_NAME="$2"
    shift
    shift
    ;;
    --registry)
    REGISTRY_HOST="$2"
    shift
    shift
    ;;
    --tag)
    IMAGE_TAG="$2"
    shift
    shift
    ;;
    --version)
    PUSH_IMAGE_VERSION="$2"
    shift
    shift
    ;;
    --latest)
    PUSH_LATEST="$2"
    shift
    shift
    ;;
    *)
    shift
    ;;
esac
done

if [[ $PUSH_IMAGE_VERSION == "none" ]]
then
    PUSH_IMAGE_VERSION=""
fi

if [ -z $IMAGE_NAME ]
then
    echo "NO IMAGE NAME passed by --image"
    exit 1
fi

if [ -z $REGISTRY_HOST ]
then
    echo "NO REGISTRY HOST passed by --registry"
    exit 1
fi

if [ -z $IMAGE_TAG ]
then
    IMAGE_TAG="nightly"
fi

IMAGE_NAME_FULL=$IMAGE_NAME:$IMAGE_TAG

push_image() {
    if [ -z $1 ]
    then
        echo "NO IMAGE NAME SPECIFIED FOR PUSH IMAGE"
        return 2
    fi

    CURRENT_PUSH_IMAGE_NAME=$1

    echo "RUNNING: \"$ docker image tag $IMAGE_NAME_FULL $CURRENT_PUSH_IMAGE_NAME\""
    docker image tag $IMAGE_NAME_FULL $CURRENT_PUSH_IMAGE_NAME

    echo "RUNNING: \"$ docker image push $CURRENT_PUSH_IMAGE_NAME > /dev/null\""
    docker image push $CURRENT_PUSH_IMAGE_NAME > /dev/null
    if [ $? -ne 0 ]
    then
        echo "ERROR RUNNING: \"$ docker image push $CURRENT_PUSH_IMAGE_NAME > /dev/null\""
        exit 1
    fi

    echo "RUNNING: \"$ docker image rm $CURRENT_PUSH_IMAGE_NAME\""
    docker image rm $CURRENT_PUSH_IMAGE_NAME

    if [ $? -ne 0 ]
    then
        echo "ERROR RUNNING: \"$ docker image rm $CURRENT_PUSH_IMAGE_NAME\""
        exit 1
    fi
}

PUSH_IMAGE_NAME="$REGISTRY_HOST/$IMAGE_NAME_FULL"
push_image $PUSH_IMAGE_NAME
if [ $PUSH_IMAGE_VERSION ]
then
    push_image "$PUSH_IMAGE_NAME-$PUSH_IMAGE_VERSION"
fi

if [ $PUSH_LATEST == "true" ]
then
    push_image "$REGISTRY_HOST/$IMAGE_NAME:latest"
fi
